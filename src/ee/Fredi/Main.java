package ee.Fredi;

import java.util.Arrays;

public class Main {


    public static void main(String[] args) {

        // ÜLESANNE 1

        double pi = Math.PI;
        System.out.println(pi * 2);

        System.out.println(ulesanne2(5, 5));

        ulesanne3();

        System.out.println(ulesanne4(0));
        System.out.println(ulesanne4(1));
        System.out.println(ulesanne4(128));
        System.out.println(ulesanne4(598));
        System.out.println(ulesanne4(1624));
        System.out.println(ulesanne4(1827));
        System.out.println(ulesanne4(1996));
        System.out.println(ulesanne4(2017));

    }


    // ÜLESANNE 2
    public static boolean ulesanne2(int a, int b) {
        if (a == b) {
            return true;
        }
        return false;
    }

    // ÜLESANNE 3
    private static void ulesanne3() {
        String[] sonad = {"üks", "kaks", "kolm", "viisteist"};

        int[] pikkus = new int[sonad.length];
        for (var sona : sonad) {
            int arv = (sona.length());
            //Arrays.fill(pikkus, arv);
            System.out.println(arv);
        }

//        for (int i = 0; i < pikkus.length; i++) {
//            System.out.println(pikkus[i]);
//        }

        //TEE ARRAY
    }

    //ÜLESANNE 4
    private static byte ulesanne4(int aastaarv) {
        if (aastaarv >= 1 && aastaarv <= 100) {
            return 1;
        } else if (aastaarv >= 101 && aastaarv <= 200) {
            return 2;
        } else if (aastaarv >= 201 && aastaarv <= 300) {
            return 3;
        } else if (aastaarv >= 301 && aastaarv <= 400) {
            return 4;
        } else if (aastaarv >= 401 && aastaarv <= 500) {
            return 5;
        } else if (aastaarv >= 501 && aastaarv <= 600) {
            return 6;
        } else if (aastaarv >= 601 && aastaarv <= 700) {
            return 7;
        } else if (aastaarv >= 701 && aastaarv <= 800) {
            return 8;
        } else if (aastaarv >= 801 && aastaarv <= 900) {
            return 9;
        } else if (aastaarv >= 901 && aastaarv <= 1000) {
            return 10;
        } else if (aastaarv >= 1001 && aastaarv <= 1100) {
            return 11;
        } else if (aastaarv >= 1101 && aastaarv <= 1200) {
            return 12;
        } else if (aastaarv >= 1201 && aastaarv <= 1300) {
            return 13;
        } else if (aastaarv >= 1301 && aastaarv <= 1400) {
            return 14;
        } else if (aastaarv >= 1401 && aastaarv <= 1500) {
            return 15;
        } else if (aastaarv >= 1501 && aastaarv <= 1600) {
            return 16;
        } else if (aastaarv >= 1601 && aastaarv <= 1700) {
            return 17;
        } else if (aastaarv >= 1701 && aastaarv <= 1800) {
            return 18;
        } else if (aastaarv >= 1801 && aastaarv <= 1900) {
            return 19;
        } else if (aastaarv >= 1901 && aastaarv <= 2000) {
            return 20;
        } else if (aastaarv >= 2001 && aastaarv <= 2018) {
            return 21;
        } else {
            return -1;
        }

    }

    // ÜLESANNE 5




}
